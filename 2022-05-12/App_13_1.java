import java.util.Map.Entry;
import java.util.*;

public class App_13_1 {
	public static void main(String[] args) {
		// 建立映射集合，向集合里添加元素
		HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(1, "Monday");
		hm.put(2, "Tuesday");
		hm.put(3, "Wednesday");
		hm.put(4, "Thusday");
		hm.put(5, "Friday");
		hm.put(6, "Saturday");
		hm.put(7, "Sunday");
		System.out.println("使用Keyset方法完成输出：");
		// 获得散列映射的键集
		Set<Integer> set1 = hm.keySet();
		Iterator<Integer> it = set1.iterator();
		while (it.hasNext()) {
			int y = it.next();
			System.out.print("\t" + y + ": " + hm.get(y));
		}
		System.out.println();
		System.out.println("使用entryset方法完成输出：");

		// 获得散列映射的映射集
		Set<Map.Entry<Integer, String>> maps = hm.entrySet();
		// Set maps = hm.entrySet();
		Iterator<Map.Entry<Integer, String>> it2 = maps.iterator();
		// Iterator it2 = maps.iterator();

		while (it2.hasNext()) {
			Entry<Integer, String> x = it2.next();
			System.out.print("\t" + x.getKey() + ": " + x.getValue());
		}
		System.out.println();
		System.out.println("使用entryset方法输出修改后的结果：");
		hm.clear();
		String hm1[] = { "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日" };

		// Iterator<Entry<Integer, String>> it3=maps.iterator();
		for (int i = 1; i < 8; i++) {
			// hm.replace(i,hm1[i-1]);
			hm.put(i, hm1[i - 1]);
			// System.out.print("\t"+i+": "+hm1[i-1]);
		}
		Iterator<Entry<Integer, String>> it3 = maps.iterator();
		while (it3.hasNext()) {
			Entry<Integer, String> z = it3.next();
			System.out.print("\t" + z.getKey() + ": " + z.getValue());
		}
	}

}