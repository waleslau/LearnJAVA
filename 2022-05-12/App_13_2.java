import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class App_13_2 {

	public static void main(String[] args) {
		ArrayList<Student> student1 = new ArrayList<>();
		ArrayList<Student> student2 = new ArrayList<>();
		ArrayList<Student> student3 = new ArrayList<>();

		Student stu11 = new Student("张三丰", 80, "男");
		Student stu12 = new Student("杨过", 22, "男");
		Student stu13 = new Student("黄蓉", 38, "女");
		Student stu21 = new Student("张三", 25, "男");
		Student stu22 = new Student("李四", 26, "男");
		Student stu23 = new Student("王五", 27, "男");
		Student stu31 = new Student("杰克", 25, "男");
		Student stu32 = new Student("露丝", 26, "男");
		Student stu33 = new Student("汤姆", 27, "男");

		Map<String, ArrayList<Student>> hashMap = new HashMap<>();

		student1.add(stu11);
		student1.add(stu12);
		student1.add(stu13);
		String key1 = "三年级一班";
		hashMap.put(key1, student1);

		student2.add(stu21);
		student2.add(stu22);
		student2.add(stu23);
		String key2 = "三年级二班";
		hashMap.put(key2, student2);

		student3.add(stu31);
		student3.add(stu32);
		student3.add(stu33);
		String key3 = "三年级三班";
		hashMap.put(key3, student3);

		Scanner scanner = new Scanner(System.in);
		System.out.println("输入班级名称：");
		String s = scanner.nextLine();
		scanner.close();

		try {
			if (hashMap.get(s) != null) {
				System.out.println(s + "学生列表");
			}

			ArrayList<Student> students = hashMap.get(s);
			for (Object value : students) {
				Student student = (Student) value;
				System.out.println(student.getName() + "   " + student.getSex() + "   " + student.getAge());
			}
		} catch (NullPointerException e) {
			System.out.println("查无此班！");
		}
		// finally {
		// System.out.println("OVER");
		// }

	}
}

class Student {

	String name;
	int age;
	String sex;

	public Student() {
	}

	public Student(String name, int age, String sex) {
		this.name = name;
		this.age = age;
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
