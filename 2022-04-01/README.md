[面向对象基础 - 廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1252599548343744/1260451488854880)

- 类的实例方法和静态方法的区别：是否有 static
  - 在一个 class 中定义的字段，我们称之为实例字段。实例字段的特点是，每个实例都有独立的字段，各个实例的同名字段互不影响。
  - 还有一种字段，是用 static 修饰的字段，称为静态字段：static field。
  - 实例字段在每个实例中都有自己的一个独立“空间”，但是静态字段只有一个共享“空间”
- 方法的重载
  - 在一个类中，我们可以定义多个方法。如果有一系列方法，它们的功能都是类似的，只有参数有所不同，那么，可以把这一组方法名做成同名方法。这种方法名相同，但各自的参数不同，称为方法重载（Overload）。
  - **一同三不同**
    - **方法名**相同
    - 参数的**个数**、**类型**、**顺序**不同
  - 参数个数或参数类型完全相同，只有返回值类型不同，不是重载，方法重载的返回值类型通常都是相同的
  - 方法重载的目的是，功能类似的方法使用同一名字，更容易记住，因此，调用起来更简单
- 构造方法
  - 创建实例的时候，实际上是通过构造方法来初始化实例
  - 构造方法的名称就是类名。构造方法的参数没有限制，在方法内部，也可以编写任意语句。但是，和普通方法相比，构造方法没有返回值（也没有 void），调用构造方法，必须用 new 操作符
  - 任何 class 都有构造方法
  - 如果自定义了一个构造方法，那么，编译器就不再自动创建默认构造方法
  - 如果既要能使用带参数的构造方法，又想保留不带参数的构造方法，那么只能把两个构造方法都定义出来
- 成员变量和静态变量
  - 所属范围不同。静态变量是属于类范围的;成员变量是属于对象范围的。
  - 存活时间不同。类的一生有着静态变量的伴随;而成员变量只能陪类走一程,对象产生的时候它就产生,而且它会随着对象的消亡而消亡。
  - 存储位置不同。静态变量时存储在方法区里的静态区;成员变量存储在堆栈内存区。
  - 调用方式不同。静态变量可以通过类名调用,也可以通过对象来调用;成员变量只能通过对象名调用。
