import java.util.Scanner;

public class Class_test05 {
    public static void main(String[] args) {
        try (Scanner reader = new Scanner(System.in)) {
            System.out.println("name: ");
            String name = reader.next();
            System.out.println("age: ");
            int age = reader.nextInt();
            Person p1 = new Person(name, age); // 既可以调用带参数的构造方法
            Person p2 = new Person(name, age); // 也可以调用无参数构造方法
            System.out.println(p1.getName());
            System.out.println(p2.getName());
        }
    }
}

class Person {
    private static String name; // 默认成员变量
    // private int age; //实例变量
    private static int age;

    public Person(String name, int age) {
        // 带参数的构造方法
        // this.name = name;
        // this.age = age;
        Person.name = name;
        Person.age = age;
    }

    public Person() {
        // 无参数构造方法（默认的就是这个）
        // 无参数构造方法内也可以调用有参构造方法
        // this();name =name;age = age;
        // 或者
        this(name, age);
    }

    public String getName() {
        return Person.name; // 调用默认成员变量
    }

    public int getAge() {
        // return this.age; //调用实例变量
        return Person.age;
    }
}