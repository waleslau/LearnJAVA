import java.util.Scanner;

public class Class_test04 {
    public static void main(String[] args) {
        try (Scanner reader = new Scanner(System.in)) {
            System.out.println("name: ");
            String name = reader.next();
            System.out.println("age: ");
            int age = reader.nextInt();
            Person p1 = new Person(name, age); // 既可以调用带参数的构造方法
            Person p2 = new Person(name, age); // 也可以调用无参数构造方法
            System.out.println(p1.getName());
            System.out.println(p2.getName());
        }
    }
}

class Person {
    private String name;
    private static int age = 0; // 类变量

    public Person(String name, int age) {
        // 带参数的构造方法
        this.name = name;
        Person.age = age;// 类变量调用
    }

    public Person() {
        // 无参数构造方法（默认的就是这个）
        // 无参数构造方法内也可以调用有参构造方法
        // this();name =name;age = age;
        // 或者
        // this(name, age);
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }
}