public class ZHSY_5_1_sale {
	public static void main(String[] args) {
		SaleTickets t = new SaleTickets();
		Thread t1 = new Thread(t, "第一售票窗口");
		Thread t2 = new Thread(t, "第二售票窗口");
		Thread t3 = new Thread(t, "第三售票窗口");
		Thread t4 = new Thread(t, "第四售票窗口");
		t1.start();
		t2.start();
		t3.start();
		t4.start();
	}
}

class SaleTickets implements Runnable {
	private int tickets = 1;

	public void run() {
		while (true) {
			if (tickets <= 100)
				System.out.println(Thread.currentThread().getName() + "销售第" + tickets++ + "票");
			else
				break;
		}
	}
}
