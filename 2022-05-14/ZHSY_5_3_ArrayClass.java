public class ZHSY_5_3_ArrayClass<T> {

	private T[] array; // 定义私有泛型数组array

	public void setT(T array[]) {
		// 定义参数为类型形参的数组array
		this.array = array;
	}

	public T[] getT() {

		return array;
	}

	public static void main(String[] args) {

		ZHSY_5_3_ArrayClass<String> a = new ZHSY_5_3_ArrayClass<String>(); // 定义类型实参为String型的泛型对象a
		String[] beauty = { "西施", "王昭君", "貂蝉", "杨贵妃" };
		a.setT(beauty);
		for (int i = 0; i < a.getT().length; i++) {

			System.out.print(a.getT()[i] + "  "); // 调用getT()方法返回数组中的值并输出
		}
	}
}
