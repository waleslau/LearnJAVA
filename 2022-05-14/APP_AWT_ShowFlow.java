import java.awt.*;

public class APP_AWT_ShowFlow extends Frame {
    public APP_AWT_ShowFlow() {
        super("FlowLayout example");// 标题
        setSize(500, 300);
        // setLayout(new FlowLayout();
        add(new Button("Button 1"));
        add(new Button("Button 2"));
        add(new Button("Button 3"));
        add(new Button("Button 4"));
        add(new Button("Button 5"));
        // addWindowListener(new WindowCloser();
        // pack()://调整窗口大小
        setVisible(true);// 显式窗口组件
    }

    public static void main(String[] args) {
        APP_AWT_ShowFlow fl = new APP_AWT_ShowFlow();
    }

}
