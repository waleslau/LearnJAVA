public class Class_test03 {
	public static void main(String[] args) {

		int a[] = { 8, 3, 7, 88, 9, 23 }; // 定义一维数组a
		LeastNumb MinNumber = new LeastNumb();
		MinNumber.least(a); // 将一维数组a传入least()方法
	}
}

class LeastNumb // 定义另一个类
{
	public void least(int array[])
	// 参数array[]接收一维整型数组
	{
		int temp = array[0];
		for (int i = 0; i < array.length; i++)
			if (temp > array[i])
				temp = array[i];
		System.out.println("最小的数为：" + temp);
	}
}