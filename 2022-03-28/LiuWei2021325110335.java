import java.util.Scanner;

public class LiuWei2021325110335 {

	public static void main(String[] args) {
		Student stuA = new Student();
		System.out.print("Please enter student information: \n");
		stuA.newStudent();
		stuA.printInfo();
		System.out.println("");
		System.out.println("#########fan APP#########");
		Fan fanA = new Fan();
		fanA.setOn(true);
		fanA.setSpeed(222);
		fanA.setRadius(18);
		fanA.setColor("red");
		System.out.println("\t###fanA###\t\n" + fanA.toString());
	}

}

class Student {
	private String name, sex, is_master;
	private double math, literature, english;
	Scanner reader = new Scanner(System.in);

	void newStudent() {
		System.out.println("name: ");
		name = reader.next();
		System.out.println("Sex(boy or girl): ");
		sex = reader.next();
		System.out.println("Is master?(yes or no): ");
		is_master = reader.next();
		System.out.println("Math scores: ");
		math = reader.nextDouble();
		System.out.println("Literature scores: ");
		literature = reader.nextDouble();
		System.out.println("English scores: ");
		english = reader.nextDouble();
	}

	double sumScore() {
		return math + literature + english;
	}

	double averageScore() {
		return sumScore() / 3;
	}

	void printInfo() {
		System.out.println("Name: " + name);
		System.out.println("Sex: " + sex);
		System.out.println("isMaster: " + is_master);
		System.out.println("Sum of scores=" + sumScore());
		System.out.println("Average of scores=" + averageScore());
	}

}

class Fan {
	private int speed;
	private boolean on;
	private double radius;
	private String color;

	public int getSpeed() {
		return this.speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public boolean isOn() {
		return this.on;
	}

	public void setOn(boolean on) {
		this.on = on;
	}

	public double getRadius() {
		return this.radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String toString() {
		return "Speed: " + speed + "\nOn: " + on + "\nRadius: " + radius + "\nColor: " + color;
	}
}