public class Class_test02 {

	public static void main(String[] args) {
		Person3 wang = new Person3();
		wang.setBirth(2008);
		System.out.println(wang.getAge());
		wang.setName("Xiao Wang");
		System.out.println(wang.getName());
		
	}

}

class Person3 {
	private String name;
	private int birth;

	public void setName(String name) {
		this.name = name; // 前面的this不可少，少了就变成局部变量name了
	}

	public String getName() {
		return name; // 相当于this.name,在不涉及参数传递、数值改变时可以不加this
	}

	public void setBirth(int birth) {
		this.birth = birth;
	}

	public int getAge() {
		return calcAge(2022); // 调用private方法
	}

	// private方法:
	private int calcAge(int currentYear) {
		return currentYear - this.birth;
	}
}