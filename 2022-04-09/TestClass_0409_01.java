public class TestClass_0409_01 {
    public static void main(String[] args) {
        System.out.println();
    }
}

class Person {
    protected String name;
    protected int age;

    public Person() {
        System.out.println("调用了Person构造方法");
    }
}

class Student extends Person {
    public Student() {
        System.out.println("调用了Student构造方法");
    }

    public String hello() {
        return "Hello, " + name; // OK!
    }
}