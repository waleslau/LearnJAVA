public class LiuWei_ShiYanSix {
	public static void main(String[] args) {
		// 1、
		System.out.println("########一########");
		Person p1 = new Person("p1", "guangdong");
		Teacher t1 = new Teacher("t1", "master");
		Teacher t2 = new Teacher("t2", "guangdong", "master");
		System.out.println("#####" + "p1" + "#####");
		p1.info();
		System.out.println("#####" + "t1" + "#####");
		t1.info();
		System.out.println("#####" + "t2" + "#####");
		t2.info();
		// 2、
		System.out.println("########二########");
		Box box1 = new Box(20, 20, 20);
		System.out.println("box1的体积：" + box1.volume());
		BoxWeight box2 = new BoxWeight(20, 20, 20, 20);
		System.out.println("box2的体积：" + box2.volume());
		System.out.println("box2的重量：" + box2.weight());
		// 3、
		System.out.println("########三########");
		Vehicle v1 = new Vehicle(20, 20);
		Bicycle b1 = new Bicycle(20, 20, 20);
		Car c1 = new Car(20, 20, 20);
		System.out.println("#####" + "v1" + "#####");
		v1.info();
		System.out.println("#####" + "b1" + "#####");
		b1.info();
		System.out.println("#####" + "c1" + "#####");
		c1.info();
	}
}

// 1、编码实现子类的构造函数的练习：
class Person {
	protected String name, location;

	// public Person() {
	// }

	public Person(String name, String location) {
		this.name = name;
		this.location = location;
	}

	public void info() {
		System.out.println("姓名：" + name);
		System.out.println("籍贯：" + location);
	}
}

class Teacher extends Person {
	private String captical;

	public Teacher(String name, String captical) {
		super(name, "河南");
		this.captical = captical;
	}

	public Teacher(String name, String location, String captical) {
		super(name, location);
		this.captical = captical;
	}

	@Override
	public void info() {
		super.info();
		System.out.println("职称：" + captical);
	}
}

// 2、父类 Box，子类 BoxWeight，计算输出子类对象的体积和重量。

class Box {
	protected double width, height, depth;

	public Box(double width, double height, double depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
	}

	public double volume() {
		return width * depth * height;
	}
}

class BoxWeight extends Box {
	private double density;

	public BoxWeight(double width, double height, double depth, double density) {
		super(width, height, depth);
		this.density = density;
	}

	public double weight() {
		return this.volume() * this.density;
	}

}

// 3、定义一个车的基类 Vehicle，含有成员 speed，weight。该类派生出自行车类 Bicycle，增加 high 高度成员；
// 派生出汽车类 Car，增加 seatnum 座位数成员。然后，编制测试类，建立 Bicycle 对象和 Car 对象，并输出它们的有关数据

class Vehicle {
	protected double speed, weight;

	public Vehicle(double speed, double weight) {
		this.speed = speed;
		this.weight = weight;
	}

	public void info() {
		System.out.println("速度：" + speed);
		System.out.println("重量：" + weight);
	}
}

class Bicycle extends Vehicle {
	private double high;

	public Bicycle(double speed, double weight, double high) {
		super(speed, weight);
		this.high = high;
	}

	public void info() {
		super.info();
		System.out.println("高度：" + high);
	}

}

class Car extends Vehicle {
	private int seatnum;

	public Car(double speed, double weight, int seatnum) {
		super(speed, weight);
		this.seatnum = seatnum;

	}

	public void info() {
		super.info();
		System.out.println("座位数：" + seatnum);
	}

}
