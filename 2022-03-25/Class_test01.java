public class Class_test01 {

	public static void main(String[] args) {
		Person hong = new Person();
		hong.name = "Xiao Hong";
		hong.age = 15;
		System.out.println(hong.name);

		Person2 ming = new Person2();
		ming.setName("Xiao Ming"); // 设置name
		ming.setAge(12); // 设置age
		System.out.println(ming.getName() + ", " + ming.getAge());

		Person3 wang = new Person3();
		wang.setBirth(2008);
		System.out.println(wang.getAge());
		wang.setName("Xiao Wang");
		System.out.println(wang.getName());

	}

}

class Person {
	public String name;
	public int age;
}

class Person2 {
	private String name;
	private int age;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		if (age < 0 || age > 100) {
			throw new IllegalArgumentException("invalid age value");
		}
		this.age = age;
	}
}

class Person3 {
	private String name;
	private int birth;

	public void setName(String name) {
		this.name = name; // 前面的this不可少，少了就变成局部变量name了
	}

	public String getName() {
		return name; // 相当于this.name
	}

	public void setBirth(int birth) {
		this.birth = birth;
	}

	public int getAge() {
		return calcAge(2022); // 调用private方法
	}

	// private方法:
	private int calcAge(int currentYear) {
		return currentYear - this.birth;
	}
}