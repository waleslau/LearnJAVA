import java.util.Scanner;

public class IsFullNum {

	public static void main(String[] args) {
		int num, sum = 0;
		Scanner reader = new Scanner(System.in);
		System.out.println("请输入一个整数：");
		num = reader.nextInt();

		for (int i = 1; i < num; i++) {
			if (num % i == 0) {
				sum += i;
			}
		}
		if (sum == num) {
			System.out.println(num + "是完全数");
		} else {
			System.out.println(num + "不是完全数");
		}

	}

}
