import java.util.Scanner;

public class HowManyDaysTheMonth {

	public static void main(String[] args) {
		int year, month, days;
		Scanner reader = new Scanner(System.in);
		System.out.println("请输入年份：");
		year = reader.nextInt();
		System.out.println("请输入月份：");
		month = reader.nextInt();
		switch (month) {
		case 2: {

			if (year % 4 == 0 && year % 100 != 0) {
				days = 29;
			} else {
				days = 28;
			}
			break;
		}
		case 4:
		case 6:
		case 9:
		case 11:
			days = 30;
			break;
		default:
			days = 31;
		}
		System.out.println(year + "年的" + month + "月份为" + days + "天");

	}

}
