import java.util.Scanner;

public class WhichMax {

	public static void main(String[] args) {
		int a, b, c, maxNum = 0;
		Scanner reader = new Scanner(System.in);
		System.out.println("请输入第一个数：");
		a = reader.nextInt();
		System.out.println("请输入第二个数：");
		b = reader.nextInt();
		System.out.println("请输入第三个数：");
		c = reader.nextInt();
		if (a > b && a > c) {
			maxNum = a;
		} else if (b > a && b > c) {
			maxNum = b;
		} else if (c > a && c > b) {
			maxNum = c;
		}
		System.out.println("最大的数是" + maxNum);
	}

}