import java.util.Random;

public class LiuWei_04 {

	public static void main(String[] args) {
//		Random random = new Random(0);
//		int[] x = {};
//		for (int i = 0; i <8; i++) {
//			int rann = random.nextInt(100);
//			x[i] = rann;
//			System.out.println(rann);
//		}
		int[] arr = { 95, 33, 99, 66, 54, 89, 12, 51 };
		System.out.println("排序前数组为：");
		for (int i : arr) {
			System.out.print(i + "\t");
		}

		// Bubble Sort
		for (int i = 0; i < arr.length - 1; i++) {
			int temp;
			for (int j = 0; j < arr.length - i - 1; j++) {
				if (arr[j + 1] < arr[j]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}

		System.out.println();
		System.out.println("排序后的数组为：");
		for (int i : arr) {
			System.out.print(i + "\t");
		}
	}

}
