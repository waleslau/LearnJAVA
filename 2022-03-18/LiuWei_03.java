import java.util.Arrays;
import java.util.Scanner;

public class LiuWei_03 {

	public static void main(String[] args) {
		int[] a = { 8, 4, 2, 1, 23, 344, 12 };
		System.out.println(" a = { 8, 4, 2, 1, 23, 344, 12 }");
		// 1
		System.out.println("1. 循环输出数列的值:");
		int m = 0;
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + "\t");
			m += a[i];
		}
		System.out.println();
		// 2
		System.out.println("2. 所有数值的和是" + m);
		// 3
		System.out.println("3. 猜数游戏：从键盘中任意输入一个数据，判断数列中是否包含此数");
		Scanner reader = new Scanner(System.in);
		System.out.println("请输入一个整数：");
		int num = reader.nextInt();
		Arrays.sort(a);
		int is_exit = Arrays.binarySearch(a, num);
		if (is_exit < 0) {
			System.out.println("数列中不包含此数");
		} else {
			System.out.println("数列中包含此数");
		}
	}

}
