/*
 * 1、编写程序，从键盘上输入一个浮点数，然后将该浮点数的整数部分输出。
 * */
import java.util.Scanner;

public class Test_20220304_01 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		double num1;
		System.out.println("输入一个浮点数：");
		num1 = reader.nextDouble();
		System.out.println((int)num1);
	}
}
