/*
 * 2、编写程序，从键盘输入两个数，然后计算它们相除后得到的结果并输出。
 * */
import java.util.Scanner;

public class Test_20220304_02 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int num1,num2;
		System.out.println("请输入被除数：");
		num1 = reader.nextInt();
		System.out.println("请输入除数：");
		num2 = reader.nextInt();
		System.out.println("商是：");
		System.out.println(num1/num2);
	}
}
