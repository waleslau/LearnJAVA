/*
 * 3、编写程序，从键盘输入圆柱体的底半径r和高h，然后计算其体积并输出(其中圆周率定义为常量，取值为3.14）。
 * */
import java.util.Scanner;

public class Test_20220304_03 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int r, h;
		System.out.println("请输入圆柱的半径r：");
		r = reader.nextInt();
		System.out.println("请输入圆柱的高h：");
		h = reader.nextInt();
		System.out.println("圆柱的面积是：");
		System.out.println(3.14 * r * r * h);
	}
}
