package ch01;

import java.io.FileReader;
import java.io.IOException;

public class Test10_2 {
	public static void main(String[] args) throws IOException {
		// TODO 自动生成的方法存根
		char[] c = new char[500]; // 创建可容纳500个字符的数组
		FileReader fr = new FileReader("C:\\Windows\\win.ini");
		fr.read(c); // 用数组接收reader读取的数据
		fr.close();
		System.out.println("文件内容如下：");
		System.out.println(c);
	}

}

