package ch01;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Test10_1 {

	public static void main(String[] args) throws IOException {
		// TODO 自动生成的方法存根
		char[] c = new char[500]; // 创建可容纳500个字符的数组
		FileReader fr = new FileReader("C:\\Users\\Administrator\\Desktop\\说明.txt");
		FileWriter fw = new FileWriter("C:\\Users\\Administrator\\Desktop\\myfile\\out.txt");
		fr.read(c); // 用数组接收reader读取的数据
		fw.write(c); // 将数组接收的数据写入到output文件
		System.out.println(c);
		fr.close();
		fw.close();
	}

}
