package ch01;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Test10_3 {

	public static void main(String[] args) throws IOException {
		// TODO 自动生成的方法存根
		String s = "abcdefghijklmnopqrstuvwxyz";
		FileWriter fw = new FileWriter("C:\\Users\\Administrator\\Desktop\\myfile\\out2.txt");
		fw.write(s); // 将数组接收的数据写入到output文件
		fw.close();
		char[] c = new char[24];
		FileReader fr = new FileReader("C:\\Users\\Administrator\\Desktop\\myfile\\out2.txt");
		fr.read(c); // 用数组接收reader读取的数据
		fr.close();
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个数字：");
		int num = sc.nextInt();
		sc.close();
		System.out.println("文件的第" + num + "个字符为：" + c[num - 1]);
	}

}
