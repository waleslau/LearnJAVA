public class ComputeTime {
	public static void main(String[] args) throws Exception {
		Car007 car01 = new Car007();
		double carspeed01 = car01.speed(30, -12, 0);
		System.out.println("car01的运行速度为" + carspeed01);
		System.out.println("car01运行1000公里使用的时间为" + 1000 / carspeed01);

		Car007 car02 = new Car007();
		double carspeed02 = car02.speed(30, -12, 0);
		System.out.println("car02的运行速度为" + carspeed02);
		System.out.println("car02运行1000公里使用的时间为" + 1000 / carspeed02);

		Plane plane01 = new Plane();
		double planespeed01 = plane01.speed(-12, 5, 0);
		System.out.println("plane01的运行速度为" + planespeed01);
		System.out.println("plane01运行1000公里使用的时间为" + 1000 / planespeed01);

		Plane plane02 = new Plane();
		double planespeed02 = plane02.speed(0, 0, 1);
		System.out.println("plane02的运行速度为" + planespeed02);
		System.out.println("plane02运行1000公里使用的时间为" + 1000 / planespeed02);
	}
}

interface Common {
	double speed(double a, double b, double c);
}

class Plane implements Common {
	public double speed(double a, double b, double c) {
		try {
			if (a + b + c < 0)
				throw new Exception();
		} catch (Exception e) {
			System.out.println("速度不能为负数!");
		}
		return a + b + c;
	}
}

class Car007 implements Common {
	public double speed(double a, double b, double c) {
		try {
			if (c == 0)
				throw new ArithmeticException();
		} catch (ArithmeticException e) {
			System.out.println("除数不能为0!");
		}
		try {
			if (a * b < 0)
				throw new Exception();
		} catch (Exception e) {
			System.out.println("速度不能为负数!");
		}
		return a * b / c;
	}
}
