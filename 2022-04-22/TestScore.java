import java.util.InputMismatchException;

public class TestScore {

	public static void main(String[] args) {
		TestScore test = new TestScore();
		int[] scores = { -9, 66, 999 };
		for (int i : scores) {
			try {
				int score = i;
				test.getScore(score);
			} catch (InputMismatchException e) {
				System.out.println(e.getMessage());
			} catch (TooLow e) {
				System.out.println("输入的分数：" + e.getScore() + " " + e.getMessage());
			} catch (TooHigh e) {
				System.out.println("输入的分数：" + e.getScore() + " " + e.getMessage());
			}
		}
	}

	public void getScore(int x) throws TooHigh, TooLow {
		if (x > 100)
			throw new TooHigh("分数不能超过100！", x);
		if (x < 0)
			throw new TooLow("分数不能小于0！", x);
		System.out.println("分数是" + x);
	}
}

class TooHigh extends Exception {
	private int score;

	TooHigh(String mess, int score) {
		super(mess);
		this.score = score;
	}

	public int getScore() {
		return score;
	}
}

class TooLow extends Exception {
	private int score;

	TooLow(String mess, int score) {
		super(mess);
		this.score = score;
	}

	public int getScore() {
		return score;
	}
}