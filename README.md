# JAVA_2022

[liaoxuefeng java](https://www.liaoxuefeng.com/wiki/1252599548343744)

```Java
byte a = 128;
a = ?
```

> 因为 java 中的自动转型，因此`System.out.println((byte)128)`输出为-128。  
> 在 java 中默认整型是 int 类型，int 类型是 4 字节，32 位。  
> 而 byte 类型是 1 字节，8 位，而 java 中的二进制都是采用补码形式存储：
>
> - 一个数为正，则它的原码、反码、补码相同
> - 一个数为负，则符号位为 1，其余各位是对原码取反，然后加 1。
>
> 就 int 类型的 128 而言： 原码为：`0000 0000 0000 0000 0000 0000 1000 0000`因为为正数，所以补码与原码相同。  
> 当`(byte)128`时，将 int 类型强制转换为 byte 类型。截断高 24 位，保留低 8 位，也就是 `1000 0000`。  
> 在`System.out.println`调用时，操作数栈会将其 boolean、byte、char 以及 short 当做 int 进行计算，此时进行的是有符号填充操作，前 24 位全部为 1，后 8 位`1000 0000`。  
> 然后补码-1 再取反：`1000 0000 0000 0000 0000 0000 1000 0000`因此`System.out.println((byte)128)`输出为-128，即`1000 0000 0000 1000 0000`。

## Links

- Eclipse：<https://mirrors.bfsu.edu.cn/eclipse/technology/epp/downloads/release/2023-03/R/eclipse-jee-2023-03-R-win32-x86_64.zip>
- 中文语言包：<https://mirrors.nju.edu.cn/eclipse//technology/babel/babel_language_packs/latest/BabelLanguagePack-eclipse-zh_4.26.0.v20230220105658.zip>
- others：
    - <http://www.eclipse.org/downloads/download.php?file=/technology/babel/babel_language_packs/latest/>
    - <https://www.oracle.com/java/technologies/downloads/>
