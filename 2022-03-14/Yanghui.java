public class Yanghui {

	public static void main(String[] args) {

		int level = 10;
		int[][] yh = new int[level][];
		System.out.println("杨辉三角：");
		for (int i = 0; i < yh.length; i++) {
			yh[i] = new int[i + 1];
		}
		yh[0][0] = 1;
		for (int i = 1; i < yh.length; i++) {
			yh[i][0] = 1;
			for (int j = 1; j < yh[i].length - 1; j++) {
				yh[i][j] = yh[i - 1][j - 1] + yh[i - 1][j];
			}
			yh[i][yh[i].length - 1] = 1;
		}
		for (int[] row : yh) {
			for (int col : row)
				System.out.print(col + " ");
			System.out.println();
		}

	}

}
