package ch01;

public class LiuWei_ShiYanSeven_1_2 {

	public static void main(String[] args) {
		Shape[] s = { new Circle(10), new Rect(2, 3), new Square(5) };
		String[] a = { "圆形", "矩形", "正方形" };
		for (int i = 0; i < s.length; i++) {
			System.out.println(a[i] + "--周长：" + s[i].girth() + ",面积：" + s[i].area());
		}
	}
}

// abstract class Shapes {
// public double width, height;// 如果访问权限改为private，抽象类怎么修改
//
// public Shapes(double width, double height) {
// this.width = width;
// this.height = height;
// }
//
// abstract double getArea();
//
// abstract double getPerimeter();
// }

interface Shape {
	public abstract double area(); // 面积

	public abstract double girth();// 周长
}

class Circle implements Shape {
	double radius; // 半径

	public Circle() {
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public double area() { // 圆面积
		return 3.14 * radius * radius;
	}

	public double girth() { // 周长
		return 2 * 3.14 * radius;
	}
}

class Rect implements Shape {
	double large; // 长
	double wide; // 宽

	public Rect() {
	}

	public Rect(double large, double wide) {
		this.large = large;
		this.wide = wide;
	}

	public double area() { // 矩形面积
		return large * wide;
	}

	public double girth() { // 周长
		return 2 * (large + wide);
	}
}

class Square extends Rect {
	double square; // 边长

	public Square() {
	}

	public Square(double square) {
		this.square = square;
	}

	public double area() { // 正方形面积
		return square * square;
	}

	public double girth() { // 周长
		return 4 * square;
	}
}