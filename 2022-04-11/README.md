# 2022-04-11

## 抽象类与抽象方法

如果父类的方法本身不需要实现任何功能，仅仅是为了定义方法签名，目的是让子类去覆写它，那么，可以把父类的方法声明为抽象方法：

```java
class Person {
    public abstract void run();
}
```

- 一个方法不能即是抽象的又是最终的
- 抽象方法只有方法头，没有方法体
- 抽象方法只能写在抽象类里

```java
abstract class Person {
    public abstract void run();
}
```

因为抽象类本身被设计成只能用于被继承，因此，抽象类可以强迫子类实现其定义的抽象方法，否则编译会报错。因此，抽象方法实际上相当于定义了“规范”。

### 面向抽象编程

当我们定义了抽象类`Person`，以及具体的`Student`、`Teacher`子类的时候，我们可以通过抽象类`Person`类型去引用具体的子类的实例：

```java
Person s = new Student();
Person t = new Teacher();
```

这种引用抽象类的好处在于，我们对其进行方法调用，并不关心`Person`类型变量的具体子类型：

```java
// 不关心Person变量的具体子类型:
s.run();
t.run();
```

同样的代码，如果引用的是一个新的子类，我们仍然不关心具体类型：

```java
// 同样不关心新的子类是如何实现run()方法的：
Person e = new Employee();
e.run();
```

这种尽量引用高层类型，避免引用实际子类型的方式，称之为面向抽象编程。

面向抽象编程的本质就是：

- 上层代码只定义规范（例如：`abstract class Person`）；
- 不需要子类就可以实现业务逻辑（正常编译）；
- 具体的业务逻辑由不同的子类实现，调用者并不关心。

## 接口

比抽象类还要抽象的纯抽象接口

### 抽象类和接口

|            | abstract class          | interface                                           |
| ---------- | :---------------------- | :-------------------------------------------------- |
| 继承       | 只能 extends 一个 class | 可以 implements 多个 interface                      |
| 字段       | 可以定义实例字段        | 不能定义实例字段                                    |
| 抽象方法   | 可以定义抽象方法        | 可以定义抽象方法                                    |
| 非抽象方法 | 可以定义非抽象方法      | 不可以定义非抽象方法，<br />但可以定义 default 方法 |

- 接口不能有任何实现了的方法，抽象类可以
- 类可以继承实现许多接口，但只能继承一个父类
- 类有严格的层次结构，接口没有
- 接口的数据成员必须初始化，必须 v 是静态成员
- 接口中的方法除了抽象方法，还可以有静态和默认，但不能有一般方法（没有 static/abstract/default 等修饰）

如果一个抽象类没有字段，所有方法全部都是抽象方法，就可以把该抽象类改写为接口

```java
interface Person {
    void run();
    String getName();
}
```

一个类只能直接继承一个父类，但可以同时实现若干个接口，变相实现了从多和父类继承

```java
class Student implements Person, Hello { // 实现了两个interface
    ...
}
```

### 接口继承

一个`interface`可以继承自另一个`interface`。`interface`继承自`interface`使用`extends`，它相当于扩展了接口的方法。例如：

```java
interface Hello {
    void hello();
}

interface Person extends Hello {
    void run();
    String getName();
}
```

此时，`Person`接口继承自`Hello`接口，因此，`Person`接口现在实际上有 3 个抽象方法签名，其中一个来自继承的`Hello`接口。

在使用的时候，实例化的对象永远只能是某个具体的子类，但总是通过接口去引用它，因为接口比抽象类更抽象：

```java
List list = new ArrayList(); // 用List接口引用具体子类的实例
Collection coll = list; // 向上转型为Collection接口
Iterable it = coll; // 向上转型为Iterable接口
```

### default 方法

在接口中，可以定义`default`方法。

实现类可以不必覆写`default`方法。`default`方法的目的是，当我们需要给接口新增一个方法时，会涉及到修改全部子类。如果新增的是`default`方法，那么子类就不必全部修改，只需要在需要覆写的地方去覆写新增方法。

`default`方法和抽象类的普通方法是有所不同的。因为`interface`没有字段，`default`方法无法访问字段，而抽象类的普通方法可以访问实例字段。

### 小结

Java 的接口（interface）定义了纯抽象规范，一个类可以实现多个接口；

接口也是数据类型，适用于向上转型和向下转型；

接口的所有方法都是抽象方法，接口不能定义实例字段；

接口可以定义`default`方法（JDK>=1.8）。

> 类比接口优先
