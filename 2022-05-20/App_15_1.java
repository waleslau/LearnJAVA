
// 界面有点乱，不过能启动。。。。。
import javax.swing.*;
import java.awt.*;

public class App_15_1 {

	public static void main(String[] args) {
		new RegisterFrame();

	}

}

// 注册界面
class RegisterFrame {
	public RegisterFrame() {
		init();
	}

	public void init() {
		JFrame frame = new JFrame("用户注册示例");
		frame.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 30));
		frame.setBounds(400, 100, 800, 300);

		Font font = new Font("Serief", Font.BOLD, 18);

		JPanel row1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 5));
		Label lb_username = new Label("用户名:");
		lb_username.setSize(50, 15);
		lb_username.setFont(font);
		row1.add(lb_username);

		JTextField jtf_username = new JTextField();
		jtf_username.setPreferredSize(new Dimension(140, 30));
		jtf_username.setFont(font);
		row1.add(jtf_username);

		Label lb_password = new Label("密码:");
		lb_password.setSize(50, 15);
		lb_password.setFont(font);
		row1.add(lb_password);

		JPasswordField jpf_password = new JPasswordField();
		jpf_password.setPreferredSize(new Dimension(140, 30));
		jpf_password.setFont(font);
		row1.add(jpf_password);

		Label lb_sex = new Label("性别:");
		lb_sex.setSize(66, 15);
		lb_sex.setFont(font);
		row1.add(lb_sex);

		ButtonGroup rbtgp = new ButtonGroup();
		JRadioButton rb1 = new JRadioButton("男");
		rb1.setFont(font);
		rb1.setSelected(true);
		JRadioButton rb2 = new JRadioButton("女");
		rb2.setFont(font);
		rbtgp.add(rb1);
		rbtgp.add(rb2);

		row1.add(rb1);
		row1.add(rb2);
		JPanel row2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 5));

		Label lb_locate = new Label("居住地:");
		lb_locate.setSize(66, 15);
		lb_locate.setFont(font);
		row2.add(lb_locate);

		JComboBox<String> cb_job = new JComboBox<>();
		cb_job.setFont(font);
		cb_job.setEnabled(true);
		cb_job.addItem("北京");
		cb_job.addItem("圣地亚哥");
		cb_job.addItem("梵蒂冈");
		row2.add(cb_job);

		Label lb_hobby = new Label("爱好:");
		lb_hobby.setSize(66, 15);
		lb_hobby.setFont(font);
		row2.add(lb_hobby);
		JCheckBox cb1 = new JCheckBox("音乐");
		cb1.setFont(font);
		JCheckBox cb2 = new JCheckBox("旅游");
		cb2.setFont(font);
		JCheckBox cb3 = new JCheckBox("上网");
		cb3.setFont(font);
		JCheckBox cb4 = new JCheckBox("运动");
		cb4.setFont(font);
		JCheckBox cb5 = new JCheckBox("购物");
		cb5.setFont(font);

		row2.add(cb1);
		row2.add(cb2);
		row2.add(cb3);
		row2.add(cb4);
		row2.add(cb5);

		JPanel row3 = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 5));

		JButton btn_register = new JButton("确定");
		btn_register.setFont(font);
		btn_register.setPreferredSize(new Dimension(130, 35));
		row3.add(btn_register);

		JButton btn_cancel = new JButton("取消");
		btn_cancel.setFont(font);
		btn_cancel.setPreferredSize(new Dimension(130, 35));
		row3.add(btn_cancel);

		frame.add(row1);
		frame.add(row2);
		frame.add(row3);

		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}
}