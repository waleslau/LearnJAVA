﻿public class Circle_2022_04_04 {

	public static void main(String[] args) {
		Circle c1 = new Circle();
		c1.setRadius(3.0);
		c1.disp();
		Circle c2 = new Circle(5.2);
		c2.disp();
	}
}

class Circle {
	private double radius;
	private final static double P = Math.PI;
	static {
		System.out.println("圆周率：" + Circle.P);
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public Circle() {

	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getP() {
		return P;
	}

	double getPerimeter() {
		return (Math.PI) * 2 * radius;
	}

	double getArea() {
		return (Math.PI) * radius * radius;
	}

	public void disp() {
		System.out.println("半径：" + radius + "\t周长：" + getPerimeter() + "\t面积：" + getArea());
	}
}