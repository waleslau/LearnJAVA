public class Complex_2022_04_04 {

	public static void main(String[] args) {
		Complex num1 = new Complex(1, 2);
		Complex num2 = new Complex(3, 4);
		System.out.println("num1=" + num2.ToString());
		System.out.println("num2=" + num2.ToString());
		Complex num3 = num1.complexAdd(num2);
		System.out.println("num1+num2=" + num3.ToString());
	}
}

class Complex {
	int RealPart, ImaginPart;

	public Complex() {
		RealPart = 0;
		ImaginPart = 0;
	}

	public Complex(int r, int i) {
		RealPart = r;
		ImaginPart = i;
	}

	Complex complexAdd(Complex a) {
		this.RealPart += a.RealPart;
		this.ImaginPart += a.ImaginPart;
		return this;
	}

	String ToString() {
		String h = RealPart + "+" + ImaginPart + "i";
		return h;
	}
}