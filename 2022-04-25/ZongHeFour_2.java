
// 未完成
import java.util.*;

import javax.swing.plaf.metal.MetalBorders.PopupMenuBorder;

import java.io.*;

public class ZongHeFour_2 {
    /**
     * 2、编写程序，接受用户输入的5个浮点数据和一个文件目录名，
     * 将这5个数据保存 在文件中，再从该文件中读取出来并且进行从大到小排序，
     * 然后再一次追加保存到文件中。
     * 
     */
    public static void main(String[] args){
        // Scanner input = new Scanner(System.in);
        // System.out.println("请输入文件路径：");
        // String file_path = input.nextLine();
        String file_path = "/home/Sync/Git/LearnJAVA/2022-04-25/testFile.txt";
        // System.out.println(file_path);

    }

    public void writeFile(String file_path) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("依次输入5个浮点数");
        String s1 = input.nextLine();
        String s2 = input.nextLine();
        String s3 = input.nextLine();
        String s4 = input.nextLine();
        String s5 = input.nextLine();
        input.close();
        // System.out.println(s1 + "\n" + s2 + "\n" + s3 + "\n" + s4 + "\n" + s5);
        File outFile = new File(file_path);
        FileOutputStream fos = new FileOutputStream(outFile);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeBytes(s1 + "\n");
        dos.writeBytes(s2 + "\n");
        dos.writeBytes(s3 + "\n");
        dos.writeBytes(s4 + "\n");
        dos.writeBytes(s5 + "\n");
        dos.writeBytes("\n");
        dos.close();
        fos.close();
    }

    public void read_Sort(String file_path) throws IOException, FileNotFoundException {
        float[] f = new float[5];
        File file = new File(file_path);
        FileInputStream fis = new FileInputStream(file);
        DataInputStream dis = new DataInputStream(fis);
        String str1, Str = "";
        while ((str1 = dis.readLine()) != null) {
            Str += str1;
        }
        String[] arr = Str.split("\n");
        dis.close();
        fis.close();
        for (int i = 0; i <= 4; i++) {
            f[i] = Float.parseFloat(arr[i]);
        }
        Arrays.sort(f);
        String s1 = f[4] + "\n";
        String s2 = f[3] + "\n";
        String s3 = f[2] + "\n";
        String s4 = f[1] + "\n";
        String s5 = f[0] + "";

        FileOutputStream fos = new FileOutputStream(file);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeBytes(s1);
        dos.writeBytes(s2);
        dos.writeBytes(s3);
        dos.writeBytes(s4);
        dos.writeBytes(s5);
        dos.close();
        fos.close();
    }

}
