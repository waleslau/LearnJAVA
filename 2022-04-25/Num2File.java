import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;
import java.io.*;
import java.util.Arrays;

public class Num2File {
    public static void main(String[] args) {
        shiyan3Frame s3 = new shiyan3Frame();
        s3.setVisible(true);
    }
}

class shiyan3Frame extends JFrame {
    private JLabel lbl1;
    private JLabel lbl2;
    private JTextField text1;
    private JTextField text2;
    private JTextField text3;
    private JTextField text4;
    private JTextField text5;
    private JTextField Text;
    private JButton btn1;
    private JButton btn2;
    private String str;

    shiyan3Frame() {
        setTitle("实验3");
        setSize(600, 400);
        this.setLayout(new GridLayout(10, 1));
        text1 = new JTextField();
        text2 = new JTextField();
        text3 = new JTextField();
        text4 = new JTextField();
        text5 = new JTextField();
        Text = new JTextField();
        lbl1 = new JLabel("输入文件名：");
        lbl2 = new JLabel("输入数字：");
        btn1 = new JButton("打开文件");
        btn2 = new JButton("发送数字");
        btn1.addActionListener(new ActionListener() { // 添加监听器,单击打开文件的按钮后产生事件
            public void actionPerformed(ActionEvent e) {
                str = Text.getText();
                System.out.println(str);
            }
        });
        btn2.addActionListener(new ActionListener() { // 添加监听器,单击打开文件的按钮后产生事件
            public void actionPerformed(ActionEvent e) {
                String s1 = text1.getText() + " ";
                String s2 = text2.getText() + " ";
                String s3 = text3.getText() + " ";
                String s4 = text4.getText() + " ";
                String s5 = text5.getText();
                float[] f = new float[5];
                try {
                    // System.out.println(str);
                    System.out.println(s1 + s2 + s3 + s4 + s5);
                    File outFile = new File(str);
                    FileOutputStream fos = new FileOutputStream(outFile);
                    DataOutputStream dos = new DataOutputStream(fos);
                    dos.writeBytes(s1);
                    dos.writeBytes(s2);
                    dos.writeBytes(s3);
                    dos.writeBytes(s4);
                    dos.writeBytes(s5);
                    dos.writeBytes("\n");
                    dos.close();
                    fos.close();

                    File inFile = new File(str);
                    FileInputStream fis = new FileInputStream(inFile);
                    DataInputStream dis = new DataInputStream(fis);
                    String str1, Str = "";
                    while ((str1 = dis.readLine()) != null) {
                        Str += str1;
                    }
                    String[] arr = Str.split(" ");
                    dis.close();
                    fis.close();
                    for (int i = 0; i <= 4; i++) {
                        f[i] = Float.parseFloat(arr[i]);
                    }
                    Arrays.sort(f);
                    s1 = f[4] + " ";
                    s2 = f[3] + " ";
                    s3 = f[2] + " ";
                    s4 = f[1] + " ";
                    s5 = f[0] + "";

                    outFile = new File(str);
                    fos = new FileOutputStream(outFile, true);// 加上true后表明是追加，不然就一直会覆盖
                    dos = new DataOutputStream(fos);
                    dos.writeBytes(s1);
                    dos.writeBytes(s2);
                    dos.writeBytes(s3);
                    dos.writeBytes(s4);
                    dos.writeBytes(s5);
                    dos.close();
                    fos.close();
                } catch (FileNotFoundException e1) {
                    System.out.println("FileStream" + e1);
                } catch (IOException e2) {
                    System.err.println("FileStream" + e2);
                }
            }
        });

        add(lbl1);
        add(Text);
        add(btn1);
        add(lbl2);
        add(text1);
        add(text2);
        add(text3);
        add(text4);
        add(text5);
        add(btn2);
    }
}
