import java.util.Scanner;

public class ZongHeFour_1 {
    /**
     * 1、用户自行定义一个异常，编程创建并抛出某个异常类的实例，运行该程序并观察 执行结果。
     * 例如: 用户密码的合法化验证，
     * 要求密码由4到6个数字组成，若长度不落在这个范围或不是由数字组成，抛出自己的异常。
     */
    public static void main(String[] args) {
        PassWord testPassWord1 = new PassWord();
        testPassWord1.Test();
    }

}

class PassWord {
    private String password;

    public PassWord() {
    }

    public PassWord(String password) {
        this.password = password;
    }

    void SetPassWord(String password) {
        this.password = password;
    }

    // 测试方法
    public void Test() {
        System.out.println("请输入密码：");
        Scanner input = new Scanner(System.in);
        String password = input.nextLine();
        this.SetPassWord(password);
        input.close();
        try {
            Check();
            System.out.println("密码合法");
        } catch (PasswordException e1) {
            System.out.println(e1.getMessage());
        }
        System.out.println("你的输入为：" + password);
    }

    // 检查输入是否有误，有误就抛错
    public void Check() throws PasswordException {
        if ((password.length() < 4 || password.length() > 6) && (!password.matches("\\d*"))) {
            throw new PasswordException("输入字符个数应为4-6个且只能输入数字！");
        }
        if (password.length() < 4 || password.length() > 6) {
            throw new PasswordException("输入字符个数应为4-6个");
        }
        if (!password.matches("\\d*")) {
            throw new PasswordException("只能输入数字！");
        }

    }

    // 自定义异常类
    public class PasswordException extends Exception {

        public PasswordException(String str) {
            super(str);
        }

        public PasswordException() {

        }
    }

}