import java.io.*;

public class App10_5 {

	public static void main(String[] args) throws IOException {
		char[] c = new char[500];
		try (FileReader fr = new FileReader("/home/Sync/Git/LearnJAVA/2022-04-25/README.md");) {
			int num = fr.read(c);
			String str = new String(c, 0, num);
			System.out.println("读取的字符个数为：" + num + "，其内容如下：");
			System.out.println(str);
		}
	}

}
