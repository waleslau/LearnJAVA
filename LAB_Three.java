// 1.根据以下需求建立模型（编写类、抽象类等）：
// 公司雇员中有程序员和项目经理，
// 程序员有姓名、工号、薪水、工作内容；
// 项目经理有姓名、工号、薪水、还有奖金、工作内容。

abstract class Employee {
    private String name, id;
    private double pay;

    public Employee(String name, String id, double pay) {
        super();
        this.name = name;
        this.id = id;
        this.pay = pay;
    }

    public abstract void work();
}

class Programmer extends Employee {
    Programmer(String name, String id, double pay) {
        super(name, id, pay); // 超类构造方法

    }

    public void work() {
        System.out.println("code.....");
    }
}

class Manager extends Employee {
    private int bonus;

    Manager(String name, String id, double pay, int bonus) {
        super(name, id, pay);// 超类构造方法
        this.bonus = bonus;
    }

    public void work() {
        System.out.println("manage.....");
    }
}

// 2.弹奏测试。要依据乐器的不同，进行相应的弹奏。
// 在main方法中进行测试乐器（Instrument）分为：钢琴(Piano)、小提琴(Violin)。
// 各种乐器的弹奏（ play ）方法各不相同。
// 编写一个测试类InstrumentTest，
// 要求：编写方法testPlay，对各种乐器进行测试。

interface Instrument {
    void play();
}

class Piano implements Instrument {
    public void play() {// 实现 Instrument 接口
        System.out.println("弹奏钢琴");
    }
}

class Violin implements Instrument {
    public void play() {// 实现 Instrument 接口
        System.out.println("弹奏小提琴");
    }
}

class InstrumentTest {
    void testPlay(Instrument i) {
        // 调用 i 对象实现接口 Instrument 的方法
        i.play();
    }
}

public class LAB_Three {

    public static void main(String[] args) {
        // 1.
        Employee w = new Programmer("jack", "C01", 20);// 定义程序员
        w.work();
        Employee w2 = new Manager("Tony", "M01", 15, 20);// 定义项目经理
        w2.work();
        // 2.
        Instrument piano = new Piano();
        Instrument violin = new Violin();
        new InstrumentTest().testPlay(piano);
        new InstrumentTest().testPlay(violin);

    }

}