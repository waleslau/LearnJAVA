public class Test02 {
    // 中断线程
    // https://www.liaoxuefeng.com/wiki/1252599548343744/1306580767211554
    // 对目标线程调用interrupt()方法可以请求中断一个线程，
    // 目标线程通过检测isInterrupted()标志获取自身是否已中断。
    // 如果目标线程处于等待状态，该线程会捕获到InterruptedException；
    // 目标线程检测到isInterrupted()为true或者捕获了InterruptedException都应该立刻结束自身线程；
    // 通过标志位判断需要正确使用volatile关键字；
    // volatile关键字解决了共享变量在线程间的可见性问题。
    public static void main(String[] args) throws InterruptedException {
        HelloThread t = new HelloThread();
        t.start();
        Thread.sleep(1);
        t.running = false; // 标志位置为false
    }
}

class HelloThread extends Thread {
    public volatile boolean running = true;

    public void run() {
        int n = 0;
        while (running) {
            n++;
            System.out.println(n + " hello!");
        }
        System.out.println("end!");
    }
}
