public class Test01 {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("main start...");
        Thread t = new Thread() {
            public void run() {
                System.out.println("thread run...");
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                }
                System.out.println("thread end.");
            }
        };
        t.start();
        // try {
        // Thread.sleep(20);
        // } catch (InterruptedException e) {
        // }
        t.join(); // main线程在启动t线程后，可以通过t.join()等待t线程结束后再继续运行：
        System.out.println("main end...");
    }
}