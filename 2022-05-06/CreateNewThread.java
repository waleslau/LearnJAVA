public class CreateNewThread {
    public static void main(String[] args) {
        // 方法一：从Thread派生一个自定义类，然后覆写run()方法：
        Thread t1 = new MyThread();
        t1.start(); // 启动新线程

        // 或者直接重写
        Thread t1_2 = new Thread() {
            public void run() {
                System.out.println("start new thread!");
            }
        };
        t1_2.start();

        // 方法二：创建Thread实例时，传入一个Runnable实例：
        Thread t2 = new Thread(new MyRunnable());
        t2.start();

        // 方法三：用Java8引入的lambda语法进一步简写为：
        Thread t3 = new Thread(() -> {
            System.out.println("start new thread!(lambda)");
        });
        t3.start();
    }

}

class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("start new thread!(MyThread)");
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("start new thread!(MyRunnable)");
    }
}