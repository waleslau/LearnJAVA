# JAVA_2022

[liaoxuefeng java 多线程](https://www.liaoxuefeng.com/wiki/1252599548343744/1304521607217185)

### 守护线程

Java 程序入口就是由 JVM 启动 main 线程，main 线程又可以启动其他线程。当所有线程都运行结束时，JVM 退出，进程结束。如果有一个线程没有退出，JVM 进程就不会退出。所以，必须保证所有线程都能及时结束。但是有一种线程的目的就是无限循环，例如，一个定时触发任务的线程如果这个线程不结束，JVM 进程就无法结束。问题是，由谁负责结束这个线程？然而这类线程经常没有负责人来负责结束它们。但是，当其他线程结束时，JVM 进程又必须要结束，怎么办？

答案是使用守护线程（Daemon Thread）。守护线程是指为其他线程服务的线程。在 JVM 中，所有非守护线程都执行完毕后，无论有没有守护线程，虚拟机都会自动退出。因此，JVM 退出时，不必关心守护线程是否已结束。如何创建守护线程呢？方法和普通线程一样，只是在调用 start()方法前，调用 setDaemon(true)把该线程标记为守护线程：

```java
Thread t = new MyThread();
t.setDaemon(true);
t.start();
```

在守护线程中，编写代码要注意：守护线程不能持有任何需要关闭的资源，例如打开文件等，因为虚拟机退出时，守护线程没有任何机会来关闭文件，这会导致数据丢失。

### 锁(同步)

- 多线程同时读写共享变量时，会造成逻辑错误，因此需要通过 synchronized 同步；
- 同步的本质就是给指定对象加锁，加锁后才能继续执行后续代码；
- 注意加锁对象必须是同一个实例；
- 对 JVM 定义的单个原子操作不需要同步。

如何使用 synchronized：

1. 找出修改共享变量的线程代码块；
2. 选择一个共享实例作为锁；
3. 使用 synchronized(lockObject) { ... }。

在使用 synchronized 的时候，不必担心抛出异常。因为无论是否有异常，都会在 synchronized 结束处正确释放锁

#### 不需要 synchronized 的操作

JVM 规范定义了几种原子操作：

基本类型（`long` 和 `double` 除外）赋值，例如：`int n = m`；
引用类型赋值，例如：`List<String> list = anotherList`。
`long` 和 `double` 是 64 位数据，JVM 没有明确规定 64 位赋值操作是不是一个原子操作，不过在 x64 平台的 JVM 是把 `long` 和 `double` 的赋值作为原子操作实现的。

### 同步方法

- 用 synchronized 修饰方法可以把整个方法变为同步代码块，synchronized 方法加锁对象是 this；
- 通过合理的设计和数据封装可以让一个类变为“线程安全”；
- 一个类没有特殊说明，默认不是 thread-safe；
- 多线程能否安全访问某个非线程安全的实例，需要具体问题具体分析。

当我们锁住的是`this`实例时，实际上可以用`synchronized`修饰这个方法。下面两种写法是等价的：

```java
public void add(int n) {
    synchronized(this) { // 锁住this
        count += n;
    } // 解锁
}
```

```java
public synchronized void add(int n) { // 锁住this
    count += n;
} // 解锁
```

因此，用`synchronized`修饰的方法就是同步方法，它表示整个方法都必须用`this`实例加锁。

### 死锁

- Java 的 synchronized 锁是可重入锁；
- 死锁产生的条件是多线程各自持有不同的锁，并互相试图获取对方已持有的锁，导致无限等待；
- 避免死锁的方法是多线程获取锁的顺序要一致。

[more](https://www.liaoxuefeng.com/wiki/1252599548343744/1306580911915042)
