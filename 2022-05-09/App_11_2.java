public class App_11_2 {
    public static void main(String[] args) {
        Customer c1 = new Customer();
        Customer c2 = new Customer();
        c1.start();
        c2.start();
    }
}

class Bank {
    private static int bank_saving = 0;

    public static void show_bank_saving() {
        System.out.println("BANK_SAVING:" + bank_saving);
    }

    public synchronized static void take(int money) {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
        }
        bank_saving += money;
        System.out.println("BANK_SAVING:" + bank_saving);
    }
}

class Customer extends Thread {
    public void run() {
        for (int i = 0; i < 3; i++) {
            Bank.take(100);
        }
    }
}