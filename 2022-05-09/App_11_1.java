public class App_11_1 {

    public static void main(String[] args) throws InterruptedException {
        Sal_Runnable c = new Sal_Runnable();
        Thread manager_a = new Thread(c, "Manager_A");
        Thread manager_b = new Thread(c, "Manager_B");
        manager_a.start();
        manager_a.join();
        manager_b.start();
    }
}

class Employer {
    private int id, salary;
    private String name;

    public Employer(int id, int salary, String name) {
        this.id = id;
        this.salary = salary;
        this.name = name;
        System.out.println(name + "'s salary is " + salary);
    }

    public int getEmpSalary() {
        return salary;
    }

    public void setEmpsalary(int salary) {
        this.salary = salary;
    }

    public void showSalary() {
        System.out.println("NAME:" + name + "\tSALARY:" + salary);
    }
}

class Sal_Runnable implements Runnable {
    Employer a = new Employer(001, 1800, "Lee");

    public void run() {
        change(200);
        System.out.println(Thread.currentThread().getName() + " add 200 yuan");
        a.showSalary();

    }

    private synchronized void change(int num) {
        int t = a.getEmpSalary();
        a.setEmpsalary((t + num));

    }

}