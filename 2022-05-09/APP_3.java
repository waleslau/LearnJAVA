public class APP_3 implements Runnable {
    static Thread MrZhang, MrLi;
    static TicketSeller MissWang;

    public void run() {
        if (Thread.currentThread() == MrZhang) { // 判断当前的线程

            MissWang.sellRegulate(20); // 调用买票的方法
        } else if (Thread.currentThread() == MrLi) {

            MissWang.sellRegulate(5);
        }
    }

    public static void main(String[] args) {
        APP_3 t = new APP_3();
        MrZhang = new Thread(t);
        MrLi = new Thread(t);
        MissWang = new TicketSeller();
        MrZhang.start(); // 启动张先生的线程
        MrLi.start(); // 启动李先生的线程
    }
}

class TicketSeller {
    int sumFive = 2, sumTwenty = 0; // 定义5元钱与20元钱的个数

    public synchronized void sellRegulate(int money) {
        if (money == 5) {
            System.out.println("Mr.Lee, your money is ok.");
        } else if (money == 20) {
            while (sumFive < 3) {
                try {
                    wait(); // 如果5元的个数少于3张，则线程等待
                } catch (InterruptedException e) {
                }
                sumFive = sumFive - 3;
                sumTwenty = sumTwenty + 1;
                System.out.println("Mr.zhang, you give me 20, return you 15");
            }
        }
        notifyAll(); // 通知等待的线程
    }
}
